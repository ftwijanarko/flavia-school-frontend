import React from 'react';

class Header extends React.Component{
    render(){
       return (
           <div>
               <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
                   <div className="container">
                        <a className="navbar-brand" href="#">Flavia School</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item active">
                                    <a className="nav-link" href="#">Login</a>
                                </li>
                                <li className="nav-item active">
                                    <a className="nav-link a-outline" href="#">Coba Gratis</a>
                                </li>
                            </ul>
                        </div>
                   </div>
                </nav>                
           </div>
       );
    }
}

export default Header;