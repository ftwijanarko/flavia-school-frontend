import React from 'react';

class Home extends React.Component{

    render(){
        return(
            <div>
                <div className="jumbotron jumbotron-fluid homepage-banner mb-5">
                    <div className="container homepage-banner-container">
                        <h1 className="display-5">Atur kegiatan belajar mengajar</h1>
                        <h1 className="display-5">lebih mudah dengan Flavia School</h1>
                        <p>
                            Solusi terlengkap untuk mendukung manajemen sekolah
                            <br />dengan aplikasi Flavia School
                        </p>
                        <p>&nbsp;</p>
                        <p>
                            <a href="#" className="a-fill">Coba Gratis Sekarang</a>
                        </p>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center">
                            <img src="https://images.unsplash.com/photo-1564988208099-409efa336a31?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" width="300px" className="img-fluid" alt="..." />
                        </div>
                        <div className="col-12 col-sm-6 col-md-9 col-lg-9 col-xl-9">
                            <h5 className="mt-3 text-muted">Flavia Learning Management System</h5>
                            <h2 className="color-palette-1">Manajemen kegiatan belajar mengajar anda berantakan?</h2>
                            <p className="text-muted pt-3 pb-3">Aplikasi learning management system Flavia menyederhanakan proses belajar mengajar anda dan mengelolanya menjadi lebih efisien</p>
                            <p>
                                <a href="#" className="a-outline">Pelajari lebih lanjut</a>
                            </p>
                        </div>
                    </div>
                    <hr className="mt-5 mb-5"/>
                    <div className="row">
                        <div className="col-12 col-sm-6 col-md-9 col-lg-9 col-xl-9 order-2 order-sm-1">
                            <h5 className="mt-3 text-muted">Flavia PPDB</h5>
                            <h2 className="color-palette-1">Repot dengan urusan PPDB?</h2>
                            <p className="text-muted pt-3 pb-3">
                                Permudah pendaftaran calon siswa baru dengan sistem PPDB Flavia, lebih modern dan efisien
                            </p> 
                            <p>
                                <a href="#" className="a-outline">Pelajari lebih lanjut</a>
                            </p>
                        </div>
                        <div className="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center order-1 order-sm-2">
                        <img src="asset/homepage/siswa-exam.jpg" width="300px" className="img-fluid" alt="..." />
                        </div>
                    </div>
                    <hr className="mt-5 mb-5"/>
                    <div className="row">
                        <div className="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center">
                        <img src="asset/homepage/pay.jpeg" width="300px" className="img-fluid" alt="..." />
                        </div>
                        <div className="col-12 col-sm-6 col-md-9 col-lg-9 col-xl-9">
                            <h5 className="mt-3 text-muted">Flavia School Billing System</h5>
                            <h2 className="color-palette-1">Pembayaran SPP anda belum cashless?</h2>
                            <p className="text-muted pt-3 pb-3">Dengan billing system flavia, permudah pembayaran biaya pendidikan secara online menggunakan transfer bank, kartu debit/kredit maupun e-wallet</p>
                            <p>
                                <a href="#" className="a-outline">Pelajari lebih lanjut</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid bg-palette-3 mt-5 pt-5 pb-3">
                    <div className="container">
                        <h3 className="text-center color-palette-1 mb-5">Pelajari manfaat Flavia School sesuai peran anda</h3>
                        <div className="row">
                            <div className="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body text-center color-palette-1">
                                        <i class="fi fi-person homepage-icon-benefit color-palette-1"></i>
                                        <h5 className="mt-3">Kepala Sekolah</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body text-center color-palette-1">
                                        <i class="fi fi-persons homepage-icon-benefit color-palette-1"></i>
                                        <h5 className="mt-3">Guru</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body text-center color-palette-1">
                                        <i class="fi fi-male homepage-icon-benefit color-palette-1"></i>
                                        <h5 className="mt-3">Siswa</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body text-center color-palette-1">
                                        <i class="fi fi-female homepage-icon-benefit color-palette-1"></i>
                                        <h5 className="mt-3">Orang Tua</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container mt-3 pt-5 pb-3 mb-3">
                    <div className="row">
                        <div className="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 text-center">
                        <img src="https://d9hhrg4mnvzow.cloudfront.net/www.mokapos.com/889b8249-cs_109q06u000000000000028.png" width="300px" className="img-fluid" alt="..." />
                        </div>
                        <div className="col-12 col-sm-6 col-md-9 col-lg-9 col-xl-9">
                            <h2 className="color-palette-1 mt-3">Punya pertanyaan?</h2>
                            <p className="text-muted pt-3 pb-3">Kami dapat membantu Anda untuk mengenal kami lebih baik <br/>dengan menyediakan langkah-langkah menggunakan Flavia School.</p>
                            <p>
                                <a href="#" className="a-outline">Pelajari lebih lanjut</a>
                            </p>
                        </div>
                    </div>
                    <h3 className="text-center color-palette-1 mb-3 mt-5">Mulai digitalisasi sekolah anda dengan Flavia</h3>
                    <p className="text-center text-muted pb-3">
                        Menjalankan manajemen sekolah bisa lebih efektif dan efisien
                        <br/>serta integrasi antar peran mempermudah pelaporan hasil belajar
                    </p>
                    <p className="text-center">
                        <a href="#" className="a-fill">Coba Gratis Sekarang</a>
                    </p>
                </div>
                <div className="container-fluid bg-palette-1 color-palette-white">
                    <div className="container pt-2 pb-2">
                        &copy; Copyright 2020 PT. Flavia Digital Indonesia
                    </div>
                </div>
            </div>
        )
    }

}

export default Home;