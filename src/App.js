import React from 'react';
import './App.css';
import Header from './component/header/Header';
import Home from './component/homepage/Home'

function App() {
  return (
    <div>
      <Header />
      <Home />
    </div>
  );
}

export default App;
